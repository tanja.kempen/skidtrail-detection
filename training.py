import torch
from metrics import iou, precision, recall


def train_one_epoch(model, train_loader, loss_fn, optimizer):
    model.train()
    average_loss = 0
    average_iou = 0
    average_pre = 0
    average_rec = 0

    for x,label in train_loader:
        optimizer.zero_grad()
        prediction = model(x)
        loss = loss_fn(prediction, label)
        loss.backward()
        optimizer.step()

        average_loss += loss.detach().item()

        with torch.no_grad():
            average_iou += iou(prediction.flatten(), label.flatten()).detach().item()
            average_pre += precision(prediction.flatten(), label.flatten()).detach().item()
            average_rec += recall(prediction.flatten(), label.flatten()).detach().item()

    return average_loss / len(train_loader),\
           average_iou / len(train_loader),\
           average_pre / len(train_loader),\
           average_rec / len(train_loader)


def validate(model, val_loader, loss_fn):
    model.eval()
    average_loss = 0
    average_iou = 0
    average_pre = 0
    average_rec = 0

    with torch.no_grad():
        for x,label in val_loader:
            prediction = model(x)
            loss = loss_fn(prediction, label)
            average_loss += loss.detach().item()

            with torch.no_grad():
                average_iou += iou(prediction.flatten(), label.flatten()).detach().item()
                average_pre += precision(prediction.flatten(), label.flatten()).detach().item()
                average_rec += recall(prediction.flatten(), label.flatten()).detach().item()

    return average_loss / len(val_loader), \
           average_iou  / len(val_loader), \
           average_pre  / len(val_loader), \
           average_rec  / len(val_loader)


def train(model, train_loader, val_loader, loss_fn, optimizer, epochs, print_every=10):
    train_loss_history = []
    train_iou_history = []
    train_pre_history = []
    train_rec_history = []
    val_loss_history = []
    val_iou_history = []
    val_pre_history = []
    val_rec_history = []

    for e in range(epochs):
        train_loss, train_iou, train_pre, train_rec = train_one_epoch(model, train_loader, loss_fn, optimizer)
        val_loss, val_iou, val_pre, val_rec = validate(model, val_loader, loss_fn)
        train_loss_history.append(train_loss)
        train_iou_history.append(train_iou)
        train_pre_history.append(train_pre)
        train_rec_history.append(train_rec)
        val_loss_history.append(val_loss)
        val_iou_history.append(val_iou)
        val_pre_history.append(val_pre)
        val_rec_history.append(val_rec)

        if e % print_every == 0:
            print("Epoch: {}\t\tTraining Loss: {}\t\tValidation Loss: {}\t\tTrain IoU: {}\t\tVal IoU: {}".format(e, train_loss, val_loss, train_iou, val_iou))

    return {"train_loss": train_loss_history, "val_loss": val_loss_history,
            "train_iou": train_iou_history, "val_iou": val_iou_history,
            "train_pre": train_pre_history, "val_pre": val_pre_history,
            "train_rec": train_rec_history, "val_rec": val_rec_history}
