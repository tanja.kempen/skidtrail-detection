import torch
import numpy as np
from torch.utils.data import Dataset
from utils import read_img, unpickle


class SegmentationDataset(Dataset):
    def __init__(self, tiles, labels, transform, device="cuda"):
        for t, l in zip(tiles, labels):
            # t = "/data/tiles/...tif" and l = "/data/labels/...tif"
            tilename = t.split('/')[-1]
            labelname = t.split('/')[-1]
            if tilename != labelname:
                raise ValueError("Tiles and labels do not correspond 1:1.")

        self.tiles  = torch.from_numpy(np.array([read_img(f, dim_ordering="CHW") for f in tiles])).to(device)
        self.labels = torch.from_numpy(np.array([read_img(f, dim_ordering="CHW") for f in labels])).to(device)
        self.transform = transform

    def __getitem__(self, i):
        return self.transform(self.tiles[i], self.labels[i])

    def __len__(self):
        return len(self.tiles)


#%%
class SegmentationDatasetPickle(Dataset):
    def __init__(self, tiles, labels, transform, bands=slice(None), device="cuda"):
        for t, l in zip(tiles, labels):
            # t = "/data/tiles/...tif" and l = "/data/labels/...tif"
            tilename = t.split('/')[-1]
            labelname = t.split('/')[-1]
            if tilename != labelname:
                raise ValueError("Tiles and labels do not correspond 1:1.")

        self.transform = transform
        self.bands = bands
        self.tiles  = torch.from_numpy(np.array([unpickle(f).astype(np.float32).transpose(2,0,1) for f in tiles])).to(device)
        self.labels = torch.from_numpy(np.array([unpickle(f).astype(np.float32).transpose(2,0,1) for f in labels])).to(device)
        # self.tiles  = [unpickle(f) for f in tiles]
        # self.labels = [unpickle(f) for f in labels]

    def __getitem__(self, i):
        return self.transform(self.tiles[i], self.labels[i])

    def __len__(self):
        return len(self.tiles)

#%%

