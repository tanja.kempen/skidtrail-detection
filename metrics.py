import torch


def iou(y_pred, y_true):
    eps = 1E-15
    intersection = (y_pred * y_true).sum()
    union = y_pred.sum() + y_true.sum() - intersection
    return (intersection + eps) / (union + eps)


def iou_with_logits(y_pred, y_true):
    output = torch.sigmoid(y_pred)
    return iou(output, y_true)


def precision(y_pred, y_true):
    eps = 1E-15
    tp = (y_pred * y_true).sum()
    fp = ((1 - y_true) * y_pred).sum()
    return tp / (tp + fp + eps)


def recall(y_pred, y_true):
    eps = 1E-15
    tp = (y_pred * y_true).sum()
    fn = (y_true * (1 - y_pred)).sum()
    return tp / (tp + fn + eps)



