from datetime import date
# glob is used to return all file paths that match a specific pattern
from glob import glob
import kornia.augmentation as aug
import matplotlib
import numpy as np
import torch
from kornia.augmentation import AugmentationSequential
from segmentation_models_pytorch import Unet
from torch.utils.data import DataLoader

from datasets import SegmentationDatasetPickle
from metrics import iou
from training import train

matplotlib.use("TkAgg") #Backend


def filter_stuff(string_list, exclude_strings):
    return [s for s in string_list if True not in [e in s for e in exclude_strings]]

#%%
lr = 5E-4 #learning rate
device = "cuda:0"  # nvidia cuda gpu (fast), set to cpu for the normal processor (slower)

# load tile and label file lists
tiles = glob("./data/tiles/*.pickle")
labels = glob("./data/labels/*.pickle")

# Band order: DTM, CHM, LRM, VDI
# In Python, ordering always starts with "0", therefore DTM=0, CHM=1, LRM=2, VDI=3
# enter bands for training, e.g. if you want to train DTM+CHM+LRM+VDI, enter 'bands = [0, 1, 2, 3]'
bands = [0, 1, 2, 3]

#If you have already created files for model inference, then exlude the location names from the training process:
tiles  = filter_stuff(tiles,  [])
labels = filter_stuff(labels, [])

N = len(tiles)  # total number of images

# sort both in the same manner
np.random.seed(1)
np.random.shuffle(tiles)
np.random.seed(1)
np.random.shuffle(labels)

training_split = 0.8
train_tiles = tiles[:int(N*training_split)]
val_tiles   = tiles[int(N*training_split):]
train_labels = labels[:int(N*training_split)]
val_labels   = labels[int(N*training_split):]

modelname = f"UNet_iou_lossfn_lr_{lr}_bands={bands}_train_split={training_split}_{date.today()}"

#%%
#Augmentation for flipping and rotating images
transform = AugmentationSequential(aug.RandomHorizontalFlip(),
                                   aug.RandomRotation(180),
                                   data_keys=["input", "mask"],
                                   keepdim=True)
#%%
training_ds = SegmentationDatasetPickle(train_tiles, train_labels, transform, device=device)
val_ds = SegmentationDatasetPickle(val_tiles, val_labels, (lambda x,y: (x,y)), device=device)

training_ds.tiles = training_ds.tiles[:, bands]
val_ds.tiles = val_ds.tiles[:, bands]

train_loader = DataLoader(training_ds, shuffle=True, batch_size=16)
val_loader   = DataLoader(val_ds, batch_size=16)

model = Unet(encoder_name="resnet18", in_channels=len(bands), activation="sigmoid")
model = model.to(device)
# model = torch.nn.DataParallel(model)

#%%
optimizer = torch.optim.Adam(model.parameters(), lr=lr)
bce = torch.nn.BCELoss()

def loss_fn(y_pred, y_true):
    return bce(y_pred, y_true) - iou(y_pred, y_true).log()

history = train(model, train_loader, val_loader, loss_fn, optimizer, epochs=40, print_every=1)

#%%
out = np.array([history["train_loss"], history["val_loss"], history["train_iou"], history["val_iou"], history["train_pre"], history["val_pre"], history["train_rec"], history["val_rec"]]).transpose()
np.savetxt(f"{modelname}.csv", out, header="training loss\t validation loss\t train iou\t val iou\t train pre\t val pre\t train rec\t val rec", delimiter="\t")

#%%
# save the model
model.to("cpu")
model.eval()
#Enter the count of bands the model was trained with at the second position in brackets:
t = torch.rand(1, 4, 224, 224)
scripted_model = torch.jit.trace_module(model, {"forward": t})
torch.jit.save(scripted_model, f"./models/{modelname}.pt")

#%%

