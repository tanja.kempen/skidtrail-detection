# Skid Trail Detection in Forests from Airborne Laser Scanning Data

- Repository created from Tanja Kempen, Faculty of Forest Sciences and Forest Ecology, Georg-August-University, Goettingen
- Python scripts supervised and Python utilities for processing geospatial raster data created by: M.Sc. Maximilian Freudenberg, Faculty of Forest Sciences and Forest Ecology, Georg-August-University, Goettingen

Supervisors:
1) Prof. Dr. Paul Magdon, Faculty of Resource Management, University of Applied Sciences and Arts (HAWK), Goettingen
2) Dr. Hans Fuchs, Faculty of Forest Sciences and Forest Ecology, Georg-August-University, Goettingen

This GIT Repository was built as part of the Master Thesis "Mapping of skid trails from airborne laser scanning data using a CNN" by Tanja Kempen, February 23, 2023

## Introduction

The detection and digital mapping of existing skid trails from remote sensing data is the basic aim of this study. Therefore, airborne laser scanning (ALS) data was analysed by a convolutional neural network (CNN). Ground truth data was received from skid trails that were manually tracked on the ground as a reference or by the GNSS systems of harvesters during timber cropping. Open ALS data from the Free State of Thuringia were processed as high-resolution digital terrain model, local relief model, canopy height model as well as a vegetation density index. A U-Net was trained with these models. As a result, this repository provides code, U-Net-Model and instructions to detect and map skid trails in mixed deciduous and coniferous forests. 

## Structure and Environment

- [Data](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/Data) shows the required folder structure to mirror locally and contains example data
- folders 'models' and 'results' need to be created locally
- [Environment](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/blob/main/environment.yml) contains the Environment description to use the scripts

## Step 1: ALS Data Processing

This step is for both creating data to train the model as well as to create normalized files for model inference

Use R Script: [SkidTrail_Preprocessing.R](https://gitlab.gwdg.de/-/ide/project/tanja.kempen/skidtrail-detection/tree/main/-/SkidTrail_Preprocessing.R/) to create the following terrain- and vegetation models:

1) Digital Terrain Model (DTM)
2) Canopy Height Model (CHM)
3) Vegetation Density Index (VDI)

- The R Script also includes instructions to use the free software [Relief Vizualisation Toolbox](https://www.zrc-sazu.si/en/rvt) to create the

4) Local Relief Model (LRM) 

- Optional next step: Merge and clip files to larger areas of interest e.g. in [QGIS](https://www.qgis.org/de/site/forusers/download.html)

- Save final [Raw_Data](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/Raw_Data) as "locationname_DTM.tif", "locationname_CHM.tif", "locationname_LRM.tif" and "locationname_VDI.tif"

If you want to train the model, you need Ground Truth data. Steps to create in GIS:
- Import or create shape file from type 'polyline' with existing skid trails
- Clip to the same area of interest as the terrain- and vegetation models described before
- buffer lines 2.1 m to both sides to create a polygon shape file
- use function 'rasterize (vector to raster)' with the same output raster size georeferenced units of the Raw_Data files e.g. 0.38 m in both width/horizontal and height/vertical resolution 
- save final [Raw_Data](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/Raw_Data) as "locationname_Trails.tif"

## Step 2: Clipping Training Data Set

Use Python Script [1_clipping.py](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/1_clipping.py) for:
- [Save](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/Data/normalized) normalized tif data of validation locations for testing the model to 'data/normalized'
- [Save](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/Data/tiles) pickled tiles of training data set to 'data/tiles'
- [Save](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/Data/labels) pickled labels of training data set to 'data/labels'

If you do not want to train a U-Net on your own and only use the trained U-Net model for inference of your area of interest, you only need the part of the script that creates the normalized tiff files:

```
# for testing the model, a normalized tiff needs to be created
# first, define a function "array to tif"
def array_to_tif(array, dst_filename, num_bands='multi', save_background=True, src_raster: str = "", transform=None,
                 crs=None): 

```
[...]
```
for (d, loc) in zip(data, location_names):
    array_to_tif(normalize_percentile(d).astype(np.float32), f"/data/normalized/{loc}.tif",
                 src_raster=f"/data/raw_data/{loc}_DTM.tif")

```

## Step 3: Training of U-Net-Model

Use Python Script [2_main.py](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/2_main.py) for:
- training of the model
- [Save](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/models) the trained model to folder 'models'


## Step 4: Inference of U-Net-Model

Use Python Script [3_inference.py](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/3_inference.py) for:
- predict skid trails from your area of interest, saved as a normalized file in 'data/normalized'
- [Save](https://gitlab.gwdg.de/tanja.kempen/skidtrail-detection/-/tree/main/results)  the raster-file-result in folder 'results'


## Step 5: Vectorisation of Raster Output

To translate the U-Net-Result to a skid trail shape file, the following QGIS steps are recommended:

1) Raster calculator: "resultfile > 0.3" filters out all predictions lower 0.3 and transforms the output file into a binary raster where all skid trail pixels will have the value "1" while non-skid-trail-pixels are "0"
2) GDAL function "Polygonize (Raster to Vector)" will transform the output into polygons
2a) Optional step: if necessary, use function "fix geometries"
3) With "Add Geometry Attributes", perimeter and area of every polygon are added to the attributes table
4) Open field calculator and add a new decimal field that divides area from perimeter
5) Filter all values below 0.5 and above 1 to delete all polygons that are not skid trails
6) "Simplify" (Area(Visvaligam) and Tolerance = 2) the polygons 
7) GRASS function v.voronoi.skeleton with maximum dangle length = 1 and checkbox activated "Do not create attribute table" extracts the center line
8) "Simplify" (Distance(Douglas-Peucker) and Tolerance = 1) to flatten the lines

Save as Shapefile
